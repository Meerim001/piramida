from rest_framework import serializers
from .models import Person, Payment


class PersonSerializer(serializers.ModelSerializer):
    user_profile = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Person
        fields = ('id', 'name', 'total_count', 'parent', 'total_payed', 'total_earned', 'user_profile')

    def create(self, validated_data):
        person_member = Person.objects.create(**validated_data)
        person_member.user_profile = self.context['request']
        person_member.save()
        return person_member


class PaymentSerializer(serializers.ModelSerializer):
    person = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = Payment
        fields = ('id', 'count', 'person')

    def create(self, validated_data):
        payment = Payment.objects.create(**validated_data)    
        person_member = Person.objects.get(user_profile=self.context['request'])
        payment.person = person_member
        payment.save()
        return payment
        

class PersonDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Person
        fields = ('id', 'name', 'total_count', 'parent', 'total_payed', 'total_earned', 'user_profile')        

