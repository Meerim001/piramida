from django.db import models
from django.conf import settings


class Person(models.Model):
    name = models.CharField(verbose_name="ФИО", max_length=64, blank=True, null=True)
    user_profile = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, null=True) 
    total_count = models.PositiveIntegerField(verbose_name="Общая сумма", default=0)
    parent = models.ForeignKey('self', on_delete = models.CASCADE, blank = True, null =True, verbose_name = "Родитель")
    total_payed = models.PositiveIntegerField(verbose_name="Вложенная сумма", default=0)
    total_earned = models.PositiveIntegerField(verbose_name="Заработок", default=0)

    def __str__(self):
        return self.name


class Payment(models.Model):
    person = models.ForeignKey(Person, on_delete = models.CASCADE, blank = True, null =True, verbose_name = "ФИО участника",related_name="person_payment")
    count = models.PositiveIntegerField(verbose_name="сумма", default=0)

    

       
