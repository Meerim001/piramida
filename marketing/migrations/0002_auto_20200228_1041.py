# Generated by Django 3.0.3 on 2020-02-28 04:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketing', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='payment',
            name='count',
        ),
        migrations.AddField(
            model_name='person',
            name='count',
            field=models.PositiveIntegerField(blank=True, default=0, null=True, verbose_name='сумма'),
        ),
    ]
