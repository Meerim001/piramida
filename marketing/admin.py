from django.contrib import admin
from .models import Person, Payment


admin.site.register(Person)
admin.site.register(Payment)


