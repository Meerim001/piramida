from django.shortcuts import render, get_object_or_404
from rest_framework.viewsets import ModelViewSet
from .models import Person, Payment
from .serializer import PersonSerializer, PaymentSerializer, PersonDetailSerializer
from rest_framework import  status, filters
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import TokenAuthentication
from .permissions import IsPersonOwnerOrGet, IsPaymentOwnerOrGet


class PersonView(ModelViewSet):
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [IsPersonOwnerOrGet]
    serializer_class = PersonSerializer
    queryset = Person.objects.all()
    lookup_field = 'pk'
    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filter_fields = ('name', 'total_count', 'parent')
    search_fields =  ('name', 'parent__name')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context

    def my_person(self, request, *args, **kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class PaymentView(ModelViewSet):  
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsPaymentOwnerOrGet] 
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()
    lookup_field = 'pk' 

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context

    def my_payment(self, request, *args, **kwargs):
        person_member = Person.objects.filter(user_profile=request.user)
        instance = Payment.objects.filter(person=person_member)
        serializer = self.get_serializer(instance, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer =  self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        person_member = Person.objects.get(user_profile=request.user)
        if person_member.parent:
            person_member.total_count += float(request.data['count'])*0.9
            person_member.parent.total_count += float(request.data['count'])*0.1
            person_member.total_payed += float(request.data['count'])
            person_member.parent.total_earned += float(request.data['count'])*0.1
            person_member.parent.save()
            person_member.save()
        else:
            person_member.total_count += float(request.data['count']) 
            person_member.total_payed += float(request.data['count'])
        person_member.save()

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    
    def perform_destroy(self, instance):
        person_member = instance.person
        if person_member.parent:
            person_member.total_count -= float(instance.count)*0.9
            person_member.parent.total_count -= float(instance.count)*0.1
            person_member.parent.total_earned -= float(instance.count)*0.1
            person_member.total_payed  -= float(instance.count)
            person_member.parent.save()
            person_member.save()
        else:
            person_member.total_count -= float(instance.count)
            person_member.total_payed  -= float(instance.count)
        person_member.save()
        instance.delete()

    def update(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payment = self.get_object()
        person_member = Person.objects.get(user_profile=request.user)
        if person_member.parent:
            person_member.total_count -= float(payment.count) * 0.9
            person_member.parent.total_count -= float(payment.count)*0.1
            person_member.total_count += float(request.data['count']) * 0.9
            person_member.parent.total_count += float(request.data['count']) * 0.1

            person_member.total_payed -= float(payment.count)
            person_member.parent.total_earned -= float(payment.count)*0.1
            person_member.total_payed += float(request.data['count']) 
            person_member.parent.total_earned += float(request.data['count'])*0.1
            person_member.save()
            person_member.parent.save()
        else: 
            person_member.total_count -= float(payment.count)
            person_member.total_count += float(request.data['count'])
            person_member.total_payed -= float(payment.count)
            person_member.total_payed += float(request.data['count'])
        person_member.save()
        serializer.save()  
        return Response(serializer.data,status=status.HTTP_200_OK)


class PersonDetailView(ModelViewSet):   
    authentication_classes = [TokenAuthentication ]
    permission_classes = [IsPersonOwnerOrGet]
    serializer_class = PersonDetailSerializer
    queryset = Person.objects.all()
    lookup_field = 'pk' 

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['request'] = self.request.user
        return context

    def my_person(self, request, *args, **kwargs):
        instance = Person.objects.get(user_profile=request.user)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)
 